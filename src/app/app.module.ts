import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'

import { EventsAppComponent } from './events-app.component';
import {EventsListComponents} from './events/events-list.component'
import {EventThumbnailComponent} from './events/event-thumbnail.component'
import {NavBarComponent} from './nav/navbar.component'
import {EventService} from './events/shared/event.service'

import { from, fromEventPattern } from 'rxjs';
import { ToastrService } from './common/toastr.service';
import { EventDetailsComponent } from './events/event-details/event-details.component';
import { Router, RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { CreateEventComponent } from './events/create-event/create-event.component';
import { Error404Component } from './error/error404.component';
import {EventRouteActivator} from './events/event-details/event-route-activator.service'
import { LoginComponent } from './events/login/login.component';
import {AuthService} from  './events/login/auth.service'



@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)

  ],
  
  declarations: [
    EventsAppComponent,EventsListComponents,EventThumbnailComponent,NavBarComponent,EventDetailsComponent,CreateEventComponent,Error404Component,LoginComponent
  ],

  providers: [EventService,ToastrService,EventRouteActivator,AuthService],
  bootstrap: [EventsAppComponent]
})
export class AppModule { }
