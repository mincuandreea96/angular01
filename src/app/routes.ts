import { EventsListComponents } from './events/events-list.component';
import { EventDetailsComponent } from './events/event-details/event-details.component';
import {Routes } from '@angular/router'
import { CreateEventComponent } from './events/create-event/create-event.component';
import { Error404Component } from './error/error404.component';
import { EventRouteActivator } from './events/event-details/event-route-activator.service';
import { LoginComponent } from './events/login/login.component';


export const appRoutes:Routes=[
    {path:'404',component:Error404Component},
    {path:'login',component:LoginComponent},
    {path:'event/create',component:CreateEventComponent,canDeactivate:['canDeactivateCreateEvent']},
    {path:'events',component:EventsListComponents},
    {path:'event/:id',component:EventDetailsComponent, canActivate:[EventRouteActivator]},
    {path:'',redirectTo:'events',pathMatch:'full'} ,
    {path:'user', loadChildren:'./user/user.module#UserModule'}
]