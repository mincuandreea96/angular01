import { Component,InputDecorator, Input } from '@angular/core'
import { IEvent } from './shared/event.model'


@Component({
selector:'event-thumbnail',
templateUrl: './event-thumbnail.component.html',
styles:[`
.bold {font-weight:bold;}
.green {color: #003300 !important;}
.pad-left {margin-left:10px;}
.well div {color:#ffa;}
`]
})
export class EventThumbnailComponent{
@Input() event:IEvent
someProperty:any="some value"

logFoo(){
    alert("foo")
}
}