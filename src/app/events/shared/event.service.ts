import { Injectable } from '@angular/core'
import { Subject } from 'rxjs'
import { IEvent } from './event.model';

@Injectable()
export class EventService {
    getEvents():IEvent[] {
        let subject=new Subject()
        setTimeout(()=>{subject.next(EVENTS);subject.complete()},100)
        return EVENTS
    }

    getEvent(id: number):IEvent {
        return EVENTS.find(event => event.id === id)
    }
}

const EVENTS:IEvent[] =
    [
        {
            id: 1,
            name: 'angular name 1',
            date: new Date('9/26/2020'),
            time: '8:00 am',
            price: 499.88,
            imageUrl: '/assets/images/img.jpeg',
            location: {
                address: 'Dambovita, 22',
                city: 'Targoviste',
                country: 'Romania'
            }

        },
        {
            id: 2,
            name: 'angular name 2',
            date: new Date('9/26/2020'),
            time: '10:00 am',
            price: 499.88,
            imageUrl: '/assets/images/img.jpeg',
            location: {

                city: 'Targoviste',
                country: 'Romania'
            }

        },
        {
            id: 3,
            name: 'angular name 3',
            date: new Date('9/26/2020'),
            time: '9:00 am',

            imageUrl: '/assets/images/img.jpeg',
            location: {
                address: 'Dambovita, 22',
                city: 'Targoviste',
                country: 'Romania'
            }

        }

    ]